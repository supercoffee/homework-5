#pragma once

#ifndef HOMEWORK_5_H
#define HOMEWORK_5_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "linkedlist/linkedlist.h"
#include "pagetable.h"

int readSetupValues(FILE * fin, int * vasSize, int * pageSize, int * pasSize);

List * createAddressList(FILE * fin);


#endif