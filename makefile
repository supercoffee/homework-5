all: lrucounter lrustack stus
lrucounter: cscd340hw5.c pagetable.c linkedlist/linkedlist.c
	gcc -g -D COUNTER pagetable.c cscd340hw5.c linkedlist/linkedlist.c -lrt -o lrucounter
lrustack: cscd340hw5.c pagetable.c linkedlist/linkedlist.c
	gcc -g -D STACK pagetable.c cscd340hw5.c linkedlist/linkedlist.c -lrt -o lrustack
stus: cscd340hw5.c pagetable.c linkedlist/linkedlist.c
	gcc -g -D STU	pagetable.c cscd340hw5.c linkedlist/linkedlist.c -lrt -o stus
