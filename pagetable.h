#pragma once
#ifndef PAGETABLE_H
#define PAGETABLE_H

#include "linkedlist/linkedlist.h"
#include <time.h>
#include <sys/timeb.h>

#define SUPPRESS_DETAILS 1

typedef struct page_table_t PageTable;

typedef struct page_table_entry_t PageTableEntry;

typedef struct results_t Results;

struct page_table_t{

	/*
		Page entries will be kept in indexed order.
	*/	
	List * pageTable;

	/*
		Frame references are double pointers to PTEs 
		in the page table list.
	*/
	List * frameTable;

	int pageSize;
	int vasPages;
	int pasPages;

	int vasBits;
	int pasBits;
	int offset;

	/*
		Stat counters
	*/
	int evictionCount;
	int pageFaults;
	int accessCount;
};

PageTable * createPageTable(int vasSize, int pageSize, int pasSize);

void pt_destroy(PageTable * table);

void printResults (int virtAdr, Results res);

void run_sim(PageTable * pt, List * addressList);

void sim_lruStack(PageTable * pt, List * addressList);

void sim_lruTimed(PageTable * pt, List * addressList);

void sim_stus(PageTable * pt, List * addressList);

int virtAdrToPage( int virtAdr,  PageTable * pt);

void insertOrEvict(PageTableEntry * pte, PageTable * pt);

void evict_lruStack(PageTableEntry * pte, PageTable * pt);

void updateCache(PageTableEntry * pte, PageTable * pt);

void evict_lruTimed(PageTableEntry * pte, PageTable * pt);

void evict_stus(PageTableEntry * pte, PageTable * pt);

#endif