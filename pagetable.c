#include "pagetable.h"



struct page_table_entry_t{
	int pageNumber;
	int frameNumber;
	int referenced;
	long lastModTime;
	PageTable * table;
};

struct results_t{
	int hit;
	int pageNumber;
	int frameNumber;
	int physAdr;
};


PageTable * createPageTable(int vasSize, int pageSize, int pasSize){

	if(vasSize < 1 || pageSize < 1|| pasSize < 1 || pageSize > vasSize
		|| pageSize > pasSize || vasSize < pasSize){
		return NULL;
	}

	int offset, vasBits, pasBits, i, vasPages, pasPages;

	PageTable * pt = (PageTable*)calloc(1, sizeof(PageTable));

	pt->pageTable = list_create();

	pt->frameTable = list_create();

	pt->pageSize = pageSize;
	pt->vasPages = vasPages = vasSize / pageSize;
	pt->pasPages = pasPages = pasSize / pageSize;

	for(i = 0; i < vasPages; i++){
		PageTableEntry * pte = (PageTableEntry *)calloc(1, sizeof(PageTableEntry));

		pte->table = pt;
		pte->pageNumber = i;
		pte->frameNumber = -1;
		pte->lastModTime = 0;
		pte->referenced = 0;

		list_add(pt->pageTable, pte);

	}


	for(i = 0; pageSize != 1; i++, pageSize = pageSize >> 1);
	offset = i;

	for(i =0; vasPages != 1; i++, vasPages = vasPages >> 1);
	vasBits = i;

	for(i = 0; pasPages != 1; i++, pasPages = pasPages >> 1);
	pasBits = i;

	pt->offset = offset;
	pt->vasBits = vasBits;
	pt->pasBits = pasBits;

	pt->evictionCount = 0;
	pt->pageFaults = 0;
	pt->accessCount = 0;

	return pt;

}

void pt_destroy(PageTable * pt){

	if(!pt){
		return;
	}

	list_destroy(pt->pageTable);

	list_destroy(pt->frameTable);

	free(pt);

}

void printResults (int virtAdr, Results res){

		#if ! SUPPRESS_DETAILS

		printf("Virtual Address: %d\n", virtAdr);

		printf("Page Number: %d\n", res.pageNumber);

		if(! res.hit){
			printf("Miss\n");
		}
		else{
			printf("Hit\n");
		}

		printf("Page frame number: %d\n", res.frameNumber);

		printf("Physical address: %d\n", res.physAdr);

		printf("=========================\n");

		#endif

}


void run_sim(PageTable * pt, List * addressList){

	Iterator * iter = list_iterator(addressList);
	int * nextAddress;

	struct timespec startTime;
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &startTime);

	Results result;

	while(iter_hasNext(iter)){

		nextAddress = (int *) iter_next(iter);

		entryForVirtAdr(*nextAddress, pt, &result);

		printResults(*nextAddress, result);

	}

	struct timespec endTime;
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &endTime);




	printf("Addresses translated: %d\n", pt->accessCount);
	printf("Page faults: %d\n", pt->pageFaults);
	printf("Page fault rate: %0.2f\n", ((double)pt->pageFaults) / pt->accessCount);
	printf("Eviction count: %d\n", pt->evictionCount);
	printf("Eviction rate: %0.2f\n", ((double)pt->evictionCount) / pt->accessCount);
	printf("Running time: %ld\n", endTime.tv_nsec - startTime.tv_nsec );


	iter_destroy(iter);
}

void sim_lruStack(PageTable * pt, List * addressList){

}

void sim_lruTimed(PageTable * pt, List * addressList){

}

void sim_stus(PageTable * pt, List * addressList){

}

int virtAdrToPage( int virtAdr,  PageTable * pt){

	int pageNum = virtAdr >> pt->offset;

	return pageNum;
}

int entryForVirtAdr( int virtAdr,  PageTable * pt, Results * res){

	List * pageTable = pt->pageTable;
	int frameNumber, physAdr, pageNumber, offset;
	int index = virtAdrToPage(virtAdr, pt);
	//update the results
	res -> pageNumber = pageNumber = index;

	PageTableEntry * pte = (PageTableEntry *)list_get(pageTable, index);

	if(pte->frameNumber >= 0){//referenced
		//update results again
		res->hit = 1;
		updateCache(pte, pt);
	}else{//not referenced
		res->hit = 0;
		insertOrEvict(pte, pt);
	}
	res->frameNumber = frameNumber = pte->frameNumber;
	offset = pt->offset;
	/*
		Shift the page number bits left by offset.

		XOR the virtual address by the page number to 
		zero out the page number bits.

		OR the result by the frame number shifted left by offset.
	*/
	physAdr = (virtAdr ^ (pageNumber << offset) ) | (frameNumber << offset);

	res->physAdr = physAdr;

	pt->accessCount++;

	return 0;

}

void insertOrEvict(PageTableEntry * pte, PageTable * pt){

	List * frameTable = pt->frameTable;
	int pasPages = pt->pasPages;

	if(list_size(frameTable) < pasPages){ //insert the page

		//alloc a pointer for reverse page entry
		PageTableEntry ** rpe = (PageTableEntry **)calloc(1, sizeof(PageTableEntry*));
		
		//point the pointer at the existing page table entry
		*rpe = pte;

		list_add(frameTable, rpe);

		pte->frameNumber = list_size(frameTable) - 1;

	}else{ //evict a page
 		
 		#ifdef STACK
 		// printf("Using LRU stack\n");
		evict_lruStack(pte, pt);
		#endif


		#ifdef COUNTER
		// printf("Using LRU timed\n");
		evict_lruTimed(pte, pt);
		#endif

		#ifdef STU
		// printf("Using Stu's algorithm \n");
		evict_stus(pte, pt);


		#endif

		pt->evictionCount++;
	}

	pt->pageFaults++;

}


void evict_lruStack(PageTableEntry * pte, PageTable * pt){
/*	
	pte is the entry we want to place.  The one
	that we evict will be chosen here. 
*/

	List * frameTable = pt->frameTable;

	PageTableEntry ** toEvict = list_dequeue(frameTable);

	pte->frameNumber = (*toEvict)->frameNumber;

	(*toEvict)->frameNumber = -1;
	(*toEvict)->referenced = 0;

	//point toEvict at the new page 
	*toEvict =  pte;

	//place the pointer at the end of the list
	list_enqueue(frameTable, toEvict);
}

void updateCache(PageTableEntry * pte, PageTable * pt){

	//for lruStack, move the reverse page entry for pte to the end of queue
	#ifdef STACK
	List * reverseTable = pt->frameTable;

	int index = pte -> frameNumber;

	PageTableEntry ** rte;

	Iterator * iter = list_iterator(reverseTable);

	while(iter_hasNext(iter)){

		rte = (PageTableEntry**)iter_next(iter);

		if((*rte)->frameNumber == index){
			iter_remove(iter);
			break;
		}

	}

	list_enqueue(reverseTable, rte);

	iter_destroy(iter);

	#endif

	/*
		Set referenced to 1
	*/
	pte->referenced = 1;


	//update access time
	struct timespec accessTime;
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &accessTime);
	pte->lastModTime = accessTime.tv_nsec;

}

PageTableEntry ** getOldestFrame(List * frameTable){

	Iterator * iter = list_iterator(frameTable);

	PageTableEntry ** oldest;
	int i;

	for(i = 0; iter_hasNext(iter); i++){

		PageTableEntry ** cur = (PageTableEntry**)iter_next(iter);
		if( i == 0){
			oldest = cur;
		}
		else{
			if((*cur)->lastModTime < (*oldest)->lastModTime){
				oldest = cur;
			}

		}
	}

	iter_destroy(iter);

	return oldest;
}

void evict_lruTimed(PageTableEntry * pte, PageTable * pt){

	List * frameTable = pt->frameTable;

	PageTableEntry ** toEvict = getOldestFrame(frameTable);

	pte->frameNumber = (*toEvict)->frameNumber;

	(*toEvict)->frameNumber = -1;
	(*toEvict)->referenced = 0;

	//point toEvict at the new page 
	*toEvict =  pte;
}


void evict_stus(PageTableEntry * pte, PageTable * pt){

	List * frameTable = pt->frameTable;
	Iterator * iter = list_iterator(frameTable);
	PageTableEntry ** cur;

	while(iter_hasNext(iter)){

		cur = (PageTableEntry**)iter_next(iter);

		if(! (*cur)->referenced ){//evict this one
			break;
		}
		else{
			/*
				Clear reference bit, send to back of queue
			*/
			(*cur)->referenced = 0;
			iter_remove(iter);
			list_enqueue(frameTable, cur);
		}
	}
	pte->frameNumber = (*cur)->frameNumber;

	(*cur)->frameNumber = -1;
	
	*cur = pte;

	iter_destroy(iter);

}

