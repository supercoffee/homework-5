import random

def main():
	#read setup.txt
	setup = open("setup.txt")
	values = setup.readlines()
	setup.close()

	if(len(values) < 3):
		return

	virtMem = int(values[0])
	blockSize = int(values[1])
	physMem = int(values[2])


	frames = physMem / blockSize

	pages = virtMem / blockSize

	# generate twice as meny addresses as page frames

	addresses = []

	for i in range(0, pages):
		addresses.append(random.randint(0, virtMem))

	outfile = open("random.txt", "w")

	for address in addresses:
		outfile.write(str(address) + "\n")

	outfile.close()

main()