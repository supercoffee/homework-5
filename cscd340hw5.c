#include "homework5.h"

int main(int argc, char * argv[]){
    
    char * testFileName;
    const char * setupFileName = "setup.txt";
    
    //parse args and test files for existence
    
    if(argc != 2){
        printf("Usage: hw5 file\n\tfile\t\tBatch file for testing\n");
        return -1;
        
    }
    else{
        testFileName = argv[1];
        if( access(testFileName, R_OK)){
            printf("File %s not found in working directory.\n", testFileName);
            return -1;
        }
        if( access(setupFileName, R_OK)){
            printf("File %s not found in working directory.\n", setupFileName);
            return -1;
        }
    }
    
    //read a file
    int vasSize, pageSize, pasSize;
    FILE * setupFile = fopen(setupFileName, "r");
    int fail = readSetupValues(setupFile, &vasSize, &pageSize, &pasSize);
    fclose(setupFile);
    if(fail){
        printf("Failed to read values from setup file. \n");
        return -1;
    }

    //build a memory structure
    PageTable * pt = createPageTable(vasSize, pageSize, pasSize);
    if(!pt){
        printf("Error creating page table!\n");
        return -1;
    }
    
    //read the test file
    FILE * testFile = fopen(testFileName, "r");
    List * addressList = createAddressList(testFile);
    fclose(testFile);
    
    // simulate memory access

    run_sim(pt, addressList);

    list_destroy(addressList);
    pt_destroy(pt);

    return 0;
}

int readSetupValues(FILE * fin, int * vasSize, int * pageSize, int * pasSize){
    int error = 0;
    error = fscanf(fin, "%d", vasSize);
    error = fscanf(fin, "%d", pageSize) & error;
    error = fscanf(fin, "%d", pasSize) & error;

    if(error == 0 ){
        return -1;
    }

    return 0;
}

List * createAddressList(FILE * fin){

    List * list = list_create();
    char buf[256];

    while(!feof(fin)){
        int temp;

        if(fgets(buf, 256, fin)){

            temp = atoi(buf);

            int * address = (int*) calloc(1, sizeof(int));

            *address = temp;

            list_add(list, address);

        }

    }

    return list;

}